<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <?php if (!in_array($user['role_id'], array(3, 4))) { ?>
                    <a href="<?= site_url('manajemen/detailpelanggaransiswa') ?>" class="btn btn-primary mb-3">Tambah <?= $title; ?></a>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover table-stipped">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Jenis Kelamin</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">No Telepon</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($pelanggaransiswa as $ps) { ?>
                                        <tr>
                                            <th scope="row"><?= $i++; ?></th>
                                            <td><?= $ps['nis']; ?></td>
                                            <td><?= $ps['nama']; ?></td>
                                            <td><?= $ps['namakelasajar']; ?></td>
                                            <td><?= $ps['namatahun']; ?></td>
                                            <td><?= $ps['jumlahskor']; ?></td>
                                            <td>
                                                <a href="<?= base_url('manajemen/detailpelanggaransiswa/' . rawurlencode($ps['nis']) . '/' . $ps['idtahun']); ?>" class="btn btn-sm btn-info">Detail</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                        <?= $this->pagination->create_links(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('manajemensekolah/pelanggaransiswa') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nis" name="nis" placeholder="Masukkan NIS" required>
                    </div>
                    <div class="form-group">
                        Tanggal Pelanggaran
                        <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal Pelanggaran" required>
                    </div>
                    <div class="form-group">
                        <select name="jenis" id="jenis" class="form-control">
                            <option value="">Jenis Pelanggaran</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="skor" name="skor" placeholder="Masukkan Skor" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Tahun Akademik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus tahun akademik ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="<?= base_url('manajemensekolah/deletePelanggaran') ?>" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>