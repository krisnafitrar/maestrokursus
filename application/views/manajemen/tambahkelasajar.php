<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>


        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <form action="<?= site_url('manajemen/formKelasAjar') ?>" method="post" id="form-data">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="kelas">Kelas</label>
                                <?php if ($status == 'Tambah') : ?>
                                    <select name="kelas" id="kelas" class="form-control" required></select>
                                <?php else : ?>
                                    <input name="text" id="kelas" class="form-control" readonly value="<?= $kelas[$kelasajar['idkelas']] ?>" />
                                    <select name="kelasedit" id="kelasedit" class="form-control"></select>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tahun">Tahun Akademik</label>
                                <select name="tahun" id="tahun" class="form-control select2" required>
                                    <?php foreach ($tahun as $v) { ?>
                                        <option value="<?= $v['idtahun'] ?>" <?= isset($kelasajar) && $kelasajar['idtahun'] == $v['idtahun'] ? 'selected' : '' ?>><?= $v['namatahun'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl">Wali Kelas</label>
                                <?php if ($status == 'Tambah') : ?>
                                    <select name="walikelas" id="walikelas" class="form-control" required></select>
                                <?php else : ?>
                                    <?php if (!empty($wali)) { ?>
                                        <input name="text" id="walikelas" class="form-control" readonly value="<?= isset($wali) && $wali['nip'] . ' - ' . $wali['namaguru'] ?>" />
                                    <?php } ?>
                                    <select name="walikelasedit" id="walikelasedit" class="form-control"></select>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        </div>
                        <input type="hidden" name="act" id="act" value="<?= $key != null ? 'edit' : 'tambah' ?>">
                        <input type="hidden" name="key" id="key" value="<?= $key != null ? $key : null ?>">
                        <div class="col" align="right">
                            <a href="<?= site_url('manajemen/kelasajar') ?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-success"><?= $status; ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    <?php if ($status == 'Tambah') : ?>
        $('#kelas').select2({
            minimumInputLength: 3,
            allowClear: true,
            placeholder: 'Masukkan kelas',
            ajax: {
                dataType: 'json',
                type: 'POST',
                url: '<?= site_url('manajemen/cariKelas/') ?>',
                delay: 250,
                data: function(params) {
                    return {
                        cari: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        $('#walikelas').select2({
            minimumInputLength: 3,
            allowClear: true,
            placeholder: 'Masukkan Wali Kelas',
            ajax: {
                dataType: 'json',
                type: 'POST',
                url: '<?= site_url('manajemen/cariGuru/') ?>',
                delay: 250,
                data: function(params) {
                    return {
                        cari: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    <?php else : ?>


        $('#kelasedit').select2({
            minimumInputLength: 3,
            allowClear: true,
            placeholder: 'Masukkan kelas baru (ubah)',
            ajax: {
                dataType: 'json',
                type: 'POST',
                url: '<?= site_url('manajemen/cariKelas/') ?>',
                delay: 250,
                data: function(params) {
                    return {
                        cari: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });


        $('#walikelasedit').select2({
            minimumInputLength: 3,
            allowClear: true,
            placeholder: 'Masukkan Wali Kelas Baru (ubah)',
            ajax: {
                dataType: 'json',
                type: 'POST',
                url: '<?= site_url('manajemen/cariGuru/') ?>',
                delay: 250,
                data: function(params) {
                    return {
                        cari: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    <?php endif; ?>
</script>