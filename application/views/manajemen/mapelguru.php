<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button class="btn btn-primary mb-3" data-type="tambahmapelguru">Tambah Mapel Guru</button>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">NIP</th>
                                        <th scope="col">Nama Guru</th>
                                        <th scope="col">Mapel</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($mapelguru as $mg) : ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= $mg['nip']; ?></td>
                                            <td><?= $mg['namaguru']; ?></td>
                                            <td><?= $mg['namamapel']; ?></td>
                                            <td>
                                                <button type="button" data-type="edit" class="btn btn-sm btn-primary" data-id="<?= $mg['idmapel'] . '/' . $mg['idguru'];  ?>">Edit</button>
                                                <button type="button" data-type="btndelete" class="btn btn-sm btn-danger" data-id="<?= $mg['idmapel'] . '/' . $mg['idguru']; ?>" data-guru="<?= $mg['idguru']; ?>">Hapus</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Kelas Ajar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus kelas ajar ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" data-type="delete" data-id="" data-guru="" class="btn btn-danger" data-dismiss="modal">Hapus</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=tambahmapelguru]').click(function() {
        location.href = '<?= site_url('manajemen/formMapelguru/'); ?>';
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('manajemen/formMapelguru/'); ?>' + id;
    });

    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('manajemen/deleteMapelGuru/') ?>' + id;
    });

    $('#datatable').DataTable();
</script>