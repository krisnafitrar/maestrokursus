<!-- ##### Single Course Intro Start ##### -->
<div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url(<?= base_url('assets/img/partner/partner.jpg') ?>);">
    <!-- Content -->
    <div class="single-course-intro-content text-center">
        <h3>Partner Maestro Kursus</h3>
        <div class="meta d-flex align-items-center justify-content-center">
            <a href="#">Maestro</a>
            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
            <a href="#">Course</a>
        </div>
        <div class="price">Free</div>
    </div>
</div>
<!-- ##### Single Course Intro End ##### -->

<!-- ##### Courses Content Start ##### -->
<div class="single-course-content section-padding-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 mx-auto">
                <div class="course--content">

                    <div class="clever-tabs-content">


                        <div class="tab-content" id="myTabContent">
                            <!-- Tab Text -->
                            <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                <div class="clever-description">
                                    <!-- FAQ -->
                                    <div class="clever-faqs">
                                        <h4>Daftar Jadi Partner</h4>
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="accordions" id="accordion" role="tablist" aria-multiselectable="true">

                                            <!-- Single Accordian Area -->
                                            <div class="panel single-accordion">
                                                <h6><a role="button" class="" aria-expanded="true" aria-controls="collapseOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Pendaftaran bagi penyedia kursus
                                                        <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                        <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                    </a></h6>
                                                <div id="collapseOne" class="accordion-content collapse show">
                                                    <p>Anda harus menaati seluruh peraturan dari Maerstro Kursus.</p>
                                                </div>
                                            </div>

                                            <form action="<?= base_url('partner') ?>" method="POST" enctype="multipart/form-data">
                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" class="collapsed" aria-expanded="true" aria-controls="collapseTwo" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">Data Pemilik
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseTwo" class="accordion-content collapse">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="namapemilik" id="namapemilik" class="form-control ml-3" placeholder="Nama Pemilik" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="number" name="noidentitas" id="noidentitas" class="form-control ml-3" placeholder="Nomor Identitas (KTP)" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <select name="jk" id="jk" class="form-control ml-3" required>
                                                                        <option value="Laki-Laki">Laki-Laki</option>
                                                                        <option value="Perempuan">Perempuan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="date" name="ttl" id="ttl" class="form-control ml-3" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="email" name="email" id="email" class="form-control ml-3" placeholder="Email Aktif" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <textarea name="alamat" id="alamat" rows="3" class="form-control ml-3" placeholder="Alamat"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label for="ktp" class="ml-3">Scan KTP</label>
                                                                    <input type="file" name="ktp" id="ktp" class="form-control ml-3" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="number" name="norek" id="norek" placeholder="No Rekening" class="form-control ml-3" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <select name="bank" id="bank" class="form-control ml-3" required>
                                                                        <option value="BRI">BRI</option>
                                                                        <option value="BCA">BCA</option>
                                                                        <option value="BNI">BNI</option>
                                                                        <option value="Mandiri">Mandiri</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="an_norek" id="an_norek" placeholder="Atas Nama (No Rekening)" class="form-control ml-3" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" aria-expanded="true" aria-controls="collapseThree" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseThree">Data Kursus
                                                            <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseThree" class="accordion-content collapse">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="namakursus" id="namakursus" class="form-control ml-3" placeholder="Nama Kursus" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <select name="idjenis" id="idjenis" class="form-control ml-3">
                                                                        <option value="">-- Pilih Jenis Kursus --</option>
                                                                        <?php foreach ($jeniskursus as $j) : ?>
                                                                            <option value="<?= $j['idjeniskursus'] ?>"><?= $j['jeniskursus'] ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="noizin" id="noizin" class="form-control ml-3" placeholder="No Izin" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="email" name="emailcenter" id="emailcenter" class="form-control ml-3" placeholder="Email Kursus" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="callcenter" id="callcenter" class="form-control ml-3" placeholder="Call Center" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="kota" id="kota" class="form-control ml-3" placeholder="Kota" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <input type="text" name="provinsi" id="provinsi" class="form-control ml-3" placeholder="Provinsi" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <textarea name="alamatkursus" id="alamatkursus" rows="3" class="form-control ml-3" placeholder="Alamat Kursus"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label for="fotokursus" class="ml-3">Foto Tempat Kursus</label>
                                                                    <input type="file" name="fotokursus" id="fotokursus" class="form-control ml-3" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary btn-block ml-3">Daftar Partner</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>