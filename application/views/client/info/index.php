<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="#">Info</a></li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Catagory ##### -->
<div class="clever-catagory bg-img d-flex align-items-center justify-content-center p-3" style="background-image: url(<?= base_url('assets/img/partner/partner.jpg') ?>);">
    <h3>Info</h3>
</div>
<section class="upcoming-events section-padding-100-0">
    <div class="container mt-5">
        <div class="d-flex justify-content-center">
            <div class="row">
                <div class="col-md-12 mb-5 text-center">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link <?= $active == "all" ? 'active' : ''  ?>" href="<?= base_url('info/index') ?>">Semua Jenis</a>
                        </li>
                        <?php foreach ($navbar as $nav) : ?>
                            <li class="nav-item">
                                <a class="nav-link <?= $active == $nav['idjeniskonten'] ? 'active' : ''  ?>" href="<?= base_url('info/index/' . $nav['idjeniskonten']) ?>"><?= $nav['jeniskonten'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Single Blog Area -->
            <?php foreach ($a_konten as $konten) : ?>
                <div class="col-12 col-md-4">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <img src="<?= base_url('assets/img/konten/' . $konten['gambar']) ?>" alt="">
                        <!-- Blog Content -->
                        <div class="blog-content">
                            <a href="<?= base_url('info/detail/' . $konten['idkonten']) ?>" class="blog-headline">
                                <h4><?= $konten['judul'] ?></h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#"><?= $konten['subjudul'] ?></a>
                            </div>
                            <p>Upload at : <?= $konten['time'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>