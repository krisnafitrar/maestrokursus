    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Info</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?= $a_info['jeniskonten'] . ' ' . $a_info['judul'] ?></li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Catagory Area Start ##### -->
    <?php

    if (empty($a_info['jumbotron'])) {
        $img = base_url('assets/img/defaultjumbotron.jpg');
    } else {
        $img = base_url('assets/img/konten/' . $a_info['jumbotron']);
    }

    ?>
    <div class="clever-catagory blog-details bg-img d-flex align-items-center justify-content-center p-3 height-400" style="background-image: url(<?= $img; ?>);">
        <div class="blog-details-headline">
            <h3><?= $a_info['judul'] ?></h3>
            <div class="meta d-flex align-items-center justify-content-center">
                <a href="#"><?= $a_info['jeniskonten'] ?></a>
            </div>
        </div>
    </div>
    <!-- ##### Catagory Area End ##### -->

    <!-- ##### Blog Details Content ##### -->
    <div class="blog-details-content section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <img src="<?= base_url('assets/img/konten/' . $a_info['gambar']) ?>" class="img-thumbnail" alt="">
                    <p class="mt-3 text-secondary">Diselenggarakan oleh : <?= $a_info['subjudul'] ?></p>
                </div>
                <div class="col-lg-8">
                    <!-- Blog Details Text -->
                    <div class="blog-details-text">
                        <?= $a_info['deskripsi'] ?>
                        <?= $a_info['info_tambahan'] ?>
                        <!-- Tags -->
                        <div class="post-tags">
                            <ul>
                                <li><a href="#"><?= $a_info['jeniskonten'] ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- ##### Blog Details Content ##### -->