     <!-- Navbar Area -->
     <div class="clever-main-menu">
         <div class="classy-nav-container breakpoint-off">
             <!-- Menu -->
             <nav class="classy-navbar justify-content-between" id="cleverNav">

                 <!-- Logo -->
                 <a class="nav-brand" href="<?= base_url('beranda') ?>"><b><?= settingSIM()['app_name'] ?></b></a>

                 <!-- Navbar Toggler -->
                 <div class="classy-navbar-toggler">
                     <span class="navbarToggler"><span></span><span></span><span></span></span>
                 </div>

                 <!-- Menu -->
                 <div class="classy-menu">

                     <!-- Close Button -->
                     <div class="classycloseIcon">
                         <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                     </div>

                     <!-- Nav Start -->
                     <div class="classynav">
                         <ul>
                             <li><a href="<?= base_url('beranda') ?>" class="<?= $nav == 'home' ? 'text-dark' : '' ?>">Home</a></li>
                             <li><a href="<?= base_url('kursus') ?>" class="<?= $nav == 'kursus' ? 'text-dark' : '' ?>">Kursus</a></li>
                             <li><a href="<?= base_url('Info') ?>" class="<?= $nav == 'info' ? 'text-dark' : '' ?>">Info</a></li>
                             <li><a href="#" class="<?= $nav == 'partner' ? 'text-dark' : '' ?>">Partner</a>
                                 <ul class="dropdown">
                                     <li><a href="<?= base_url('partner') ?>" class="<?= $nav == 'partner' ? 'text-dark' : '' ?>">Daftar Partner</a></li>
                                 </ul>
                             </li>
                             <li><a href="<?= base_url('kontak') ?>">Kontak</a></li>
                         </ul>

                         <!-- Search Button -->
                         <div class="search-area">
                             <form action="#" method="post">
                                 <input type="search" name="search" id="search" placeholder="Search">
                                 <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                             </form>
                         </div>

                         <!-- Register / Login -->
                         <?php if (!$this->session->userdata('username')) : ?>
                             <div class="register-login-area">
                                 <a href="<?= base_url('auth/daftar') ?>" class="btn">Daftar</a>
                                 <a href="<?= base_url('auth') ?>" class="btn active">Login</a>
                             </div>
                         <?php else : ?>
                             <div class="login-state d-flex align-items-center">
                                 <div class="user-name mr-30">
                                     <div class="dropdown">
                                         <a class="dropdown-toggle" href="#" role="button" id="userName" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('username') ?></a>
                                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userName">
                                             <a class="dropdown-item" href="<?= base_url('profil/index') ?>">Profil & Data</a>
                                             <a class="dropdown-item" href="<?= base_url('kursus/mycourse') ?>">Kursus Saya</a>
                                             <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="userthumb">
                                     <img src="<?= base_url('assets/img/user.png') ?>" alt="">
                                 </div>
                             </div>
                         <?php endif ?>

                     </div>
                     <!-- Nav End -->
                 </div>
             </nav>
         </div>
     </div>
     </header>
     <!-- ##### Header Area End ##### -->