    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?= base_url('assets/img/home.jpg'); ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2 style="text-shadow: 2px 2px 4px #000000;">Kursus Saya</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-lg-12">
                                <?= $this->session->flashdata('message'); ?>
                                <form action="" method="post" id="table-list">
                                    <table class="table table-hover mt-5" id="data-table">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama Paket</th>
                                                <th scope="col">Tempat Kursus</th>
                                                <th scope="col">Tanggal</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($mycourse as $mc) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $mc['namapaketkursus']; ?></td>
                                                    <td><?= $mc['namakursus']; ?></td>
                                                    <td><?= $mc['waktu']; ?></td>
                                                    <td class="<?= empty($mc['noregistrasi']) ? 'text-danger' : '' ?>"><?= empty($mc['noregistrasi']) ? 'Belum Bayar' : 'Sudah Bayar' ?></td>
                                                    <td>
                                                        <button data-type="btn-tagihan" type="button" class="btn btn-sm btn-secondary" data-id="<?= $mc['invoice']; ?>" data-toggle="tooltip" title="Tagihan"><i class="fa fa-database"></i></button>
                                                        <button data-type="btn-show" type="button" class="btn btn-sm btn-primary" data-id="<?= $mc['iddaftar']; ?>" data-toggle="tooltip" title="Detail"><i class="fa fa-eye"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                            <?php if (empty($mycourse)) : ?>
                                                <tr>
                                                    <td colspan="6" align="center">Anda belum mengikuti kursus apapun.</td>
                                                </tr>
                                            <?php endif ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('[data-type=btn-tagihan]').click(function() {
            var invoice = $(this).attr('data-id');
            location.href = '<?= base_url('kursus/pembayaran/') ?>' + invoice;
        });

        $('[data-type=btn-show]').click(function() {
            var id = $(this).attr('data-id');
            location.href = '<?= base_url('kursus/detailmycourse/') ?>' + id;
        });
    </script>