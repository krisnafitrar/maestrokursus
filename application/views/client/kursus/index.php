<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="#">Kursus</a></li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Catagory ##### -->
<div class="clever-catagory bg-img d-flex align-items-center justify-content-center p-3" style="background-image: url(<?= base_url('assets/img/partner/partner.jpg') ?>);">
    <h3>Daftar Paket Kursus</h3>
</div>

<div class="container mt-5">
    <div class="d-flex justify-content-center">
        <div class="row">
            <div class="col-md-12 mb-5 text-center">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link <?= $active == "all" ? 'active' : ''  ?>" href="<?= base_url('kursus/index') ?>">Semua Jenis</a>
                    </li>
                    <?php foreach ($navbar as $nav) : ?>
                        <li class="nav-item">
                            <a class="nav-link <?= $active == $nav['idjeniskursus'] ? 'active' : ''  ?>" href="<?= base_url('kursus/index/' . $nav['idjeniskursus']) ?>"><?= $nav['jeniskursus'] ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- ##### Popular Course Area Start ##### -->
<section class="popular-courses-area section-padding-100">
    <div class="container">
        <div class="row">
            <!-- Single Popular Course -->
            <?php foreach ($paketkursus as $pk) : ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <img src="<?= base_url('assets/doc/datapaket/' . $pk['foto']) ?>" alt="">
                        <!-- Course Content -->
                        <div class="course-content">
                            <h4></h4>
                            <div class="meta d-flex align-items-center">
                                <table>
                                    <tr>
                                        <td><a href="#"><?= $pk['namainstruktur'] ?></a></td>
                                    </tr>
                                </table>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <a href="<?= base_url('kursus/detail/' . $pk['idpaketkursus']) ?>">
                                            <h5><?= $pk['namapaketkursus'] ?></h5>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h5 class="text-primary"><?= toRupiah($pk['harga']) ?></h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b class="text-secondary"><?= $pk['kota'] ?></b></td>
                                </tr>
                            </table>
                            <p align="justify"><?= substr($pk['deskripsi'], 0, 142) . ' ...'; ?></p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">
                            <div class="seat-rating h-100 d-flex align-items-center">
                                <div class="seat">
                                    <i class="fa fa-user" aria-hidden="true"></i> 10
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i> 4.5
                                </div>
                                <div class="course-fee h-100">
                                    <p class="mt-2 text-primary"><b><?= $pk['jeniskursus'] ?></b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if (empty($paketkursus)) : ?>
            <div class="row d-flex justify-content-center">
                <div class="text-center mb-5">
                    <span class="text-danger"><b>Paket kursus tidak ditemukan</b></span>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>