<div class="container mt-2 mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <?= $this->session->flashdata('message') ?>
            <form action="<?= base_url('profil/password') ?>" method="POST">
                <div class="form-group">
                    <label for="oldpassword">Password Sekarang</label>
                    <input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Masukkan password sekarang" required>
                </div>
                <div class="form-group">
                    <label for="oldpassword">Password Baru</label>
                    <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Masukkan password baru" required>
                </div>
                <div class="form-group">
                    <label for="oldpassword">Konfirmasi Password Baru</label>
                    <input type="password" class="form-control" name="newpasswordconf" id="newpasswordconf" placeholder="Ulangi password" required>
                </div>
                <button type="submit" data-type="save" class="btn btn-primary btn-block">Simpan</button>
            </form>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>