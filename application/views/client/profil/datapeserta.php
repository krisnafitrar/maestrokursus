<div class="container mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8">
            <?= $this->session->flashdata('message') ?>
            <form action="<?= base_url('profil/data') ?>" method="POST">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" value="<?= $peserta['namapeserta'] ?>" required>
                </div>
                <div class="form-group">
                    <label for="nama">Email</label>
                    <input type="email" class="form-control" name="email" id="email" value="<?= $peserta['email'] ?>" required>
                </div>
                <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <input type="text" class="form-control" name="telepon" id="telepon" value="<?= $peserta['notelp'] ?>" required>
                </div>
                <div class="form-group">
                    <label for="jk">Jenis Kelamin</label>
                    <select class="form-control" name="jk" id="jk" required>
                        <?php foreach ($jeniskelamin as $key => $val) : ?>
                            <option value="<?= $key ?>"><?= $val ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" class="form-control" id="alamat" rows="3" placeholder="Masukkan alamat" required><?= $peserta['alamat'] ?></textarea>
                </div>
                <input type="hidden" value="<?= $peserta['idpeserta'] ?>" name="idpeserta">
                <button type="submit" data-type="save" class="btn btn-primary btn-block">Simpan</button>
            </form>
        </div>
    </div>
</div>
<script>
    $('#jk').val('<?= $peserta['jeniskelamin'] ?>');
</script>