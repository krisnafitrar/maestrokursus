<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" data-type="tambah" class="btn btn-primary mb-3">Tambah Guru Baru</button>
                <button type="button" class="btn btn-info mb-3" data-type="upload"><i class="fa fa-upload"></i> Import</button>

                <form action="<?= site_url('master/guru'); ?>" method="post">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" name="keyword" class="form-control" placeholder="Cari guru ..." aria-label="Cari guru ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                <div class="input-group-append" id="button-addon4">
                                    <input class="btn btn-success" type="submit" name="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <?= $this->session->flashdata('submenu'); ?>
                        <?= $this->session->flashdata('submenu_delete'); ?>
                        <h5>Hasil : <?= $total_rows; ?></h5>
                        <form method="post" id="form-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">NIP</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Jenis Kelamin</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">No Telp</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($total_rows == 0) : ?>
                                        <tr class="table table-danger">
                                            <td colspan="7" align="center">Data tidak ditemukan</td>
                                        </tr>
                                    <?php else : ?>
                                        <?php $i = 1; ?>
                                        <?php foreach ($guru as $g) : ?>
                                            <tr>
                                                <th scope="row"><?= ++$start; ?></th>
                                                <td><?= $g['nip']; ?></td>
                                                <td><?= $g['namaguru']; ?></td>
                                                <td><?= convertJK($g['jeniskelamin']); ?></td>
                                                <td><?= $g['alamat']; ?></td>
                                                <td><?= $g['notlp']; ?></td>
                                                <td>
                                                    <button type="button" data-type="edit" class="btn btn-sm btn-info" data-id="<?= $g['idguru']; ?>">Edit</button>
                                                    <button type="button" data-type="btndelete" class="btn btn-sm btn-danger" data-id="<?= $g['idguru']; ?>">Delete</a>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                        <?= $this->pagination->create_links(); ?>
                    </div>
                </div>

                <?php
                function convertJK($jk)
                {
                    return ($jk == 'L') ? 'Laki-Laki' : 'Perempuan';
                }
                ?>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Modal -->
        <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newMenuModalLabel">Tambah Guru Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('master/guru') ?>" method="post" id="modal_post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                            </div>
                            <div class="form-group">
                                <select name="jk" id="jk" class="form-control">
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="notlp" name="notlp" placeholder="No Telepon">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                        </div>
                        <input type="hidden" name="act" id="act">
                        <input type="hidden" name="key" id="key">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="modal_upload" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?= base_url('master/guru') ?>" method="post" id="form_upload" enctype='multipart/form-data'>
                    <div class="modal-header">
                        <h5 class="modal-title">Import Guru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label">File upload</label>
                            <div class="col-md-8">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fileupload" name="fileupload">
                                    <label class="custom-file-label" for="fileupload">Pilih File</label>
                                </div>
                            </div>
                        </div>
                        <p>
                            Silakan mendownload file template import disini : <button data-type="download" class="btn btn-sm btn-info">Download Template</button>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-type="import" class="btn btn-success">Import</button>
                    </div>
                    <input type="hidden" name="act" id="act">
                    <input type="hidden" name="key" id="key">
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Hapus Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda ingin menghapus guru?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('[data-type=simpan]').click(function() {
            var act = $('#modal_post #act').val();
            var key = $('#modal_post #key').val();
            if (act == "") {
                $('#modal_post #act').val('simpan');
            }
            $('#modal_post').submit();
        });


        $('[data-type=tambah]').click(function() {
            var modal = $('#newMenuModal');
            $('#modal_post')[0].reset();
            modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
            modal.find('#nip').attr('disabled', false);
            modal.modal();
        });

        $('[data-type=upload]').click(function() {
            $('#modal_upload').modal();
        });

        $('[data-type=download]').click(function() {
            $('#form_upload #act').val('download');
            $('#form_upload').submit();
        });

        $('[data-type=import]').click(function() {
            $('#form_upload #act').val('import');
            $('#form_upload').submit();
        });

        $('[data-type=btndelete]').click(function() {
            var id = $(this).attr('data-id');
            $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
            $('#modal-delete').modal();
        });

        $('[data-type=delete]').click(function() {
            var id = $(this).attr('data-id');
            location.href = '<?= site_url('master/deleteGuru?id=') ?>' + id;
        });

        $('[data-type=edit]').click(function() {
            var id = $(this).attr('data-id');
            Swal.showLoading();
            xhrfGetData("<?= site_url('master/getGuru/') ?>" + id, function(data) {
                var modal = $('#newMenuModal');
                modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
                modal.find('#nip').val(data.nip).attr('disabled', true);
                modal.find('#nama').val(data.namaguru);
                modal.find('#jk').val(data.jeniskelamin);
                modal.find('#alamat').val(data.alamat);
                modal.find('#notlp').val(data.notlp);
                modal.find('#act').val('edit');
                modal.find('#key').val(data.idguru);
                Swal.close();
                modal.modal();
            });
        });
    </script>