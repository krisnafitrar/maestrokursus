<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" data-type="tambah" class="btn btn-primary mb-3">Tambah <?= $title; ?></button>

                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover" id="tabel-mapel">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Pengaturan</th>
                                        <th scope="col">Value</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah <?= $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('master/mapel') ?>" method="post" id="modal_post">
                <div class="modal-body">

                    <div class="form-group">
                        ID <?= $title; ?>
                        <input type="text" class="form-control" id="idmapel" name="idmapel" placeholder="ID Mapel" required>
                    </div>
                    <div class="form-group">
                        Nama <?= $title; ?>
                        <input type="text" class="form-control" id="namamapel" name="namamapel" placeholder="Nama Mapel" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Mapel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus mapel ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();
        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });

    $('[data-type=tambah]').click(function() {
        var modal = $('#newMenuModal');
        $('#modal_post')[0].reset();
        modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
        modal.modal();
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData("<?= site_url('master/getMapel/') ?>" + id, function(data) {
            var modal = $('#newMenuModal');
            modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
            modal.find('#namamapel').val(data.namamapel);
            modal.find('#act').val('edit');
            modal.find('#key').val(encodeURIComponent(data.idmapel));
            Swal.close();
            modal.modal();
        });
    });

    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('master/deleteMapel?id=') ?>' + id;
    });

    $('#tabel-mapel').DataTable();
</script>