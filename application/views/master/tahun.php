<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-3" data-type="tambah">Tambah <?= $title; ?></button>

                <div class="row">
                    <div class="col-md-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <?php foreach ($a_kolom as $key => $val) {
                                            if ($val['kolom'] == ':no') { ?>
                                                <th scope="col">No</th>
                                            <?php } else { ?>
                                                <th scope="col"><?= $val['label'] ?></th>
                                            <?php } ?>
                                        <?php } ?>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($a_data as $row) { ?>
                                        <tr>
                                            <?php
                                                foreach ($a_kolom as $key => $val) {
                                                    if ($val['kolom'] == ':no') { ?>
                                                    <td scope="row"><?= $i++ ?></th>
                                                    <?php } else if (isset($val['type']) && $val['type'] == 'S') {
                                                                $option = $val['option'];
                                                                ?>
                                                    <td><?= $option[$row[$val['kolom']]] ?></th>
                                                    <?php } else { ?>
                                                    <td><?= $row[$val['kolom']]; ?></td>
                                                <?php } ?>
                                            <?php } ?>
                                            <td>
                                                <?php if (isset($a_buttonlinear)) {
                                                        foreach ($a_buttonlinear as $k => $v) { ?>
                                                        <button type="button" data-type="<?= $v['type'] ?>" data-id="<?= $row[$primary]; ?>" class="btn btn-<?= $v['class'] ?>" <?= $v['add'] ?>><?= $v['label'] ?></button>
                                                <?php }
                                                    } ?>
                                                <button type="button" data-type="edit" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-info">Ubah</button>
                                                <button type="button" data-type="btndelete" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-danger">Hapus</button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah <?= $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url($action) ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <?php
                    foreach ($a_kolom as $key => $val) {
                        if ($val['kolom'] == ':no') {
                            continue;
                        } else if (isset($val['type']) && $val['type'] == 'S') { ?>
                            <?= $val['label'] ?>
                            <select name="<?= $val['kolom'] ?>" id="<?= $val['kolom'] ?>" class="<?= $val['class'] ?>" <?= $val['add'] ?>>
                                <?php foreach ($val['option'] as $k => $v) { ?>
                                    <option value="<?= $k ?>"><?= $v ?></option>
                                <?php } ?>
                            </select>
                        <?php } else { ?>
                            <?= $val['label'] ?>
                            <input type="text" name="<?= $val['kolom'] ?>" id="<?= $val['kolom'] ?>" class="<?= $val['class'] ?>" <?= $val['add'] ?>>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                </div>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Tahun Akademik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus tahun akademik ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" data-type="delete" data-id="" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>


<script>
    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();
        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });

    $('[data-type=tambah]').click(function() {
        var modal = $('#newMenuModal');
        $('#modal_post')[0].reset();
        modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
        modal.modal();
    });

    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('master/deleteTahun?id='); ?>' + id;
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData("<?= site_url($detail) ?>" + id, function(data) {
            var modal = $('#newMenuModal');
            modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
            <?php
            foreach ($a_kolom as $key => $val) {
                if ($val['kolom'] == ':no') {
                    continue;
                } else { ?>
                    modal.find('#<?= $val['kolom'] ?>').val(data.<?= $val['kolom'] ?>);
            <?php }
            } ?>
            modal.find('#act').val('edit');
            modal.find('#key').val(data.<?= $primary ?>);
            Swal.close();
            modal.modal();
        });
    });

    $('#datatable').DataTable();
</script>