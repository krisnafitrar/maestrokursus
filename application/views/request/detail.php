<div class="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td width="150px"><b>No Identitas</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['noidentitas'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Nama Pemilik</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['namapemilik'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Tanggal Lahir</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['ttl'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Jenis Kelamin</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['jk'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Email</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>No Rekening</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['norekening'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Bank</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['bank'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Atas Nama</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['an_norek'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Alamat</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <?= $detail['alamat'] ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td width="150px"><b>No Izin</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['noizin'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Nama Kursus</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['namakursus'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Jenis Kursus</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['jeniskursus'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Call Center</b></td>
                                        <td width="50px">:</td>
                                        <td><?= $detail['callcenter'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Email Center</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <?= $detail['emailcenter'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Kota</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <?= $detail['kota'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Provinsi</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <?= $detail['provinsi'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Alamat</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <?= $detail['alamatkursus'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Status</b></td>
                                        <td width="50px">:</td>
                                        <td>
                                            <b>
                                                <?php
                                                $status = $detail['is_accepted'];

                                                if ($status == 0) {
                                                    echo '<div class="text-danger">Ditolak</div>';
                                                } else if ($status == 1) {
                                                    echo '<div class="text-warning">Pengajuan</div>';
                                                } else {
                                                    echo '<div class="text-success">Diterima</div>';
                                                }
                                                ?>
                                            </b>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row mt-3 mb-3">
                            <div class="col-md-6">
                                <img class="img-thumbnail" src="<?= base_url('assets/img/datapartner/') . $detail['foto_ktp'] ?>" alt="">
                            </div>
                            <div class="col-md-6">
                                <img class="img-thumbnail" src="<?= base_url('assets/img/datapartner/') . $detail['fotokursus'] ?>" alt="">
                            </div>
                        </div>
                        <div class="row-mt-3">
                            <?php if ($detail['is_accepted'] == 0) : ?>
                                <button type="button" data-type="btn-acc" class="btn btn-success btn-block">Terima jadi partner</button>
                            <?php elseif ($detail['is_accepted'] == 1) : ?>
                                <button type="button" data-type="btn-acc" class="btn btn-success btn-block">Terima jadi partner</button>
                                <button type="button" data-type="btn-tolak" class="btn btn-danger btn-block">Tolak jadi partner</button>
                            <?php else : ?>
                                <button type="button" data-type="btn-tolak" class="btn btn-danger btn-block">Tolak jadi partner</button>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-type=btn-acc]').click(function() {
        var idtempatkursus = '<?= $detail['idtempatkursus'] ?>';
        var status = 2;
        window.open('<?= base_url('request/printPDF/') ?>' + idtempatkursus + '/' + status);
        location.href = '<?= base_url('request/setdata/') ?>' + idtempatkursus + '/' + status;
    });

    $('[data-type=btn-tolak]').click(function() {
        var idtempatkursus = '<?= $detail['idtempatkursus'] ?>';
        var status = 0;
        window.open('<?= base_url('request/printPDF/') ?>' + idtempatkursus + '/' + status);
        location.href = '<?= base_url('request/setdata/') ?>' + idtempatkursus + '/' + status;
    });
</script>