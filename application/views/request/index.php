<?php

function getColor($params)
{
    if ($params == 0) {
        return 'table-danger';
    } else if ($params == 1) {
        return 'table-warning';
    } else {
        return 'table-success';
    }
}

function getStatus($params)
{
    if ($params == 0) {
        $color = 'table-danger';
        return 'Ditolak';
    } else if ($params == 1) {
        $color = 'table-warning';
        return 'Pengajuan';
    } else {
        $color = 'table-success';
        return 'Diterima';
    }
}

?>
<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->session->flashdata('message'); ?>
                        <form action="" method="post" id="table-list">
                            <table class="table table-hover" id="data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Kursus</th>
                                        <th scope="col">Jenis</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Tgl Daftar</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($request as $req) : ?>
                                        <tr class="<?= getColor($req['is_accepted']) ?>">
                                            <th scope="row"><?= $i; ?></th>
                                            <td><?= $req['namakursus']; ?></td>
                                            <td><?= $req['jeniskursus']; ?></td>
                                            <td><?= $req['emailcenter']; ?></td>
                                            <td><?= date('d-M-Y, H:i', $req['tgldaftar']); ?></td>
                                            <td><?= getStatus($req['is_accepted']) ?></td>
                                            <td>
                                                <button data-type="detail" type="button" class="btn btn-sm btn-info btn-block" data-id="<?= $req['idtempatkursus']; ?>">Detail</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus Submenu?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" data-id="" data-type="delete" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= base_url('request/detail/') ?>' + id;
    });

    $('#data-table').DataTable();
</script>