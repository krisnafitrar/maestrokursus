<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Laporan</title>
</head>

<body>
    <h1 align="center">Maestro Indonesia</h1>
    <h4 align="center"><u>SURAT KEPUTUSAN</u></h4>
    <br>
    <br>
    <table>
        <tr>
            <td colspan="4">Kepada Yth</td>
        </tr>
        <tr>
            <td width="50px">Ibu/Bpk</td>
            <td colspan="3"><?= $detail['namapemilik'] ?></td>
        </tr>
        <tr>
            <td colspan="4">di <?= $detail['kota'] ?></td>
        </tr>
    </table>
    <br>
    <p align="justify" style="line-height: 2;">
        Kami selaku manajer <?= settingSIM()['company_name'] ?> mengucapkan terima kasih atas apresiasi yang bapak/ibu berikan kepada perusahaan kami dengan mengirimkan surat pengajuan untuk menjadi partner beberapa hari yang lalu. Dengan melihat, meninjau dan mempertimbangkan dari kelengkapan data dan ke absahan data dari saudara yang sudah terlampirkan maka kami memutuskan bahwa <b><?= $msg ?></b>. Demikian surat ini kami buat. Terima kasih atas perhatian dan pengertiannya.
    </p>
    <?php if ($status > 1) : ?>
        Kami akan memberikan akun untuk mengakses dan mengelola kursus anda. <br>
        <br>
        <table>
            <tr>
                <td width="50px"><b>Username</b></td>
                <td>:</td>
                <td><?= $username ?></td>
            </tr>
            <tr>
                <td width="50px"><b>Password</b></td>
                <td>:</td>
                <td><?= $password ?></td>
            </tr>
        </table>
    <?php endif ?>
    <br>
    <p align="right" style="margin-top: 200px">
        Hormat Kami
        <br>
        <p align="right" style="margin-top: 50px"><?= settingSIM()['ceo_company'] ?>
            <br>
            <b><u>CEO PT.Maestro Indonesia</u></b>
        </p>
    </p>
</body>

</html>