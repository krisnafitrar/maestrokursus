<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover" id="tabel-mapel">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Pengaturan</th>
                                        <th scope="col">Value</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num = 1 ?>
                                    <?php foreach ($a_kolom as $col) : ?>
                                        <tr>
                                            <td><?= $num++ ?></td>
                                            <td><?= $col['label'] ?></td>
                                            <?php if ($col['type'] == 'F') : ?>
                                                <td><a href="<?= base_url(substr($col['path'], 1) . $a_data[$col['kolom']]) ?>"><?= $a_data[$col['kolom']] ?></a></td>
                                            <?php else : ?>
                                                <td><?= $a_data[$col['kolom']] ?></td>
                                            <?php endif ?>
                                            <td>
                                                <button type="button" data-label="<?= $col['label'] ?>" data-kolom="<?= $col['kolom'] ?>" data-type="btn-ubah" data-tipe="<?= $col['type'] ?>" data-id="<?= $a_data['idtempatkursus'] ?>" class="btn btn-success btn-block">Ubah</button>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Ubah <?= $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('setting/index') ?>" enctype="multipart/form-data" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" id="labelform"></label>
                        <input type="text" class="form-control" id="inputan" name="data" placeholder="Masukkan data" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" data-type="simpan" class="btn btn-success">Simpan</button>
                </div>
                <input type="hidden" name="col" id="col">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
    </div>
</div>


<script>
    $('[data-type=btn-ubah]').click(function() {
        var modal = $('#newMenuModal');
        var id = $(this).attr('data-id');
        var label = $(this).attr('data-label');
        var kolom = $(this).attr('data-kolom');
        var type = $(this).attr('data-tipe');
        var col = $('#col').val(kolom);
        var key = $('#key').val(id);

        modal.find('#labelform').html(label);
        if (type == 'F') {
            modal.find('#inputan').attr('name', 'file');
            modal.find('#inputan').attr('type', 'file');
        }

        modal.modal();
    });
</script>