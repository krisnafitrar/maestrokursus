<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MApi_users extends MY_Model {
    protected $table = 'users_alfian';
    protected $schema = 'test';
    protected $key = 'iduser';

    
    public function get($data){
        $where = $this->getCondition($data);
        
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

    public function listuser(){
        return $this->db->get($this->getTable());
    }
}
