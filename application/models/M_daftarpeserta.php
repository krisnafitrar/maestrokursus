<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_daftarpeserta extends MY_Model
{
    protected $table = 'daftar_peserta';
    protected $schema = '';
    public $key = 'iddaftar';
    public $value = 'noregistrasi';

    function __construct()
    {
        parent::__construct();
    }

    public function getReference($id = null)
    {
        $where = empty($id) ? "" : " WHERE iddaftar=$id";
        $query = "SELECT * FROM $this->table JOIN pembayaran USING(iddaftar) JOIN tempatkursus USING(idtempatkursus) JOIN paketkursus pk USING(idpaketkursus) JOIN jeniskursus jk ON pk.idjeniskursus=jk.idjeniskursus JOIN peserta USING(idpeserta)" . $where;
        return $this->db->query($query);
    }

    public function getKursus($id = null)
    {
        $where = empty($id) ? "" : " WHERE idpeserta=$id";
        $query = "SELECT * FROM $this->table JOIN pembayaran USING(iddaftar) JOIN tempatkursus USING(idtempatkursus) JOIN paketkursus pk USING(idpaketkursus) JOIN jeniskursus jk ON pk.idjeniskursus=jk.idjeniskursus JOIN peserta USING(idpeserta)" . $where;
        return $this->db->query($query);
    }
}
