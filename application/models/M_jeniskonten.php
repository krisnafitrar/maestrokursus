<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jeniskonten extends MY_Model
{
    protected $table = 'jeniskonten';
    protected $schema = '';
    public $key = 'idjeniskonten';
    public $value = 'jeniskonten';

    function __construct()
    {
        parent::__construct();
    }
}
