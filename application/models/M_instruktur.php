<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_instruktur extends MY_Model
{
    protected $table = 'instruktur';
    protected $schema = '';
    public $key = 'idinstruktur';
    public $value = 'namainstruktur';

    function __construct()
    {
        parent::__construct();
    }

    public function getInstruktur($limit = null)
    {
        $query = "SELECT * FROM instruktur JOIN tempatkursus USING(idtempatkursus)";
        return $this->db->query($query);
    }
}
