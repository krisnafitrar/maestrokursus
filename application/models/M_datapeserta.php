<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_datapeserta extends MY_Model
{
    protected $table = 'peserta';
    protected $schema = '';
    public $key = 'idpeserta';
    public $value = '';

    function __construct()
    {
        parent::__construct();
    }
}
