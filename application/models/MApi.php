<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MApi extends MY_Model {

    public function query1(){
        $query = $this->db->Query("select a.user_id, a.name, count(a.user_id) as jumlahpost, c.jumlahgroup, d.awal, d.akhir
            from (select *
                from (select gm.user_id, u.name, gm.group_id
                    from group_members gm
                    join users u on u.id = gm.user_id
                    where gm.role='A'
                    ) a
                where a.user_id not in (select user_id from university_users
                    where user_id is not null
                    group by user_id)) a
            join (select *
                from posts p
                where to_char(p.created_at, 'YYYY') >= '2018') b on b.user_id=a.user_id and b.group_id=a.group_id
            left join (
                select user_id, count(1) as jumlahgroup
                from group_members
                where role = 'A'
                group by user_id
                ) c on c.user_id=a.user_id
            left join (
                select user_id, max(created_at) as akhir, min(created_at) as awal from posts group by user_id
                ) d on d.user_id=a.user_id
            group by a.user_id, a.name, c.jumlahgroup, d.awal, d.akhir
            order by user_id");
        
        return $query;
    }

}