<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_komentar extends MY_Model
{
    protected $table = 'komentar';
    protected $schema = '';
    public $key = 'idkomentar';
    public $value = 'komentar';

    function __construct()
    {
        parent::__construct();
    }

    public function getKomentar($id)
    {
        $query = "SELECT * FROM $this->table JOIN peserta USING(idpeserta) WHERE idpaketkursus=$id";
        return $this->db->query($query);
    }
}
