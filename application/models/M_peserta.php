<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_peserta extends MY_Model
{
    protected $table = 'peserta';
    protected $schema = '';
    public $key = 'idpeserta';
    public $value = 'namapeserta';

    function __construct()
    {
        parent::__construct();
    }
}
