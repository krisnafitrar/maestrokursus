<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_access_menu extends MY_Model
{
    protected $table = 'user_access_menu';
    protected $schema = '';
    protected $key = 'id';

    function __construct()
    {
        parent::__construct();
    }
}
