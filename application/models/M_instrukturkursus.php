<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_instrukturkursus extends MY_Model
{
    protected $table = 'instruktur_kursus';
    protected $schema = '';
    public $key = 'id';
    public $value = 'idinstruktur';

    function __construct()
    {
        parent::__construct();
    }

    public function getReference($id)
    {
        $query = "SELECT * FROM $this->table JOIN instruktur USING(idinstruktur) WHERE idpaketkursus=$id";
        return $this->db->query($query);
    }
}
