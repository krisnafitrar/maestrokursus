<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pemilik extends MY_Model
{
    protected $table = 'pemilik';
    protected $schema = '';
    public $key = 'idowner';
    public $value = 'namapemilik';

    function __construct()
    {
        parent::__construct();
    }

    public function getAllData($id)
    {
        $q = "SELECT * FROM pemilik p JOIN tempatkursus tk USING(idtempatkursus) JOIN jeniskursus jk ON tk.idjeniskursus=jk.idjeniskursus WHERE p.idtempatkursus='$id'";
        return $this->db->query($q);
    }
}
