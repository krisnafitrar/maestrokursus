<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_submenu extends MY_Model
{
    protected $table = 'user_sub_menu';
    protected $schema = '';
    public $key = 'id';
    public $value = 'title';

    public function getSubMenuByMenu($idmenu)
    {
        return $this->db->from($this->table . ' usm')
            ->join('user_menu um', 'usm.menu_id = um.id')
            ->where(array('usm.menu_id' => $idmenu, 'usm.is_active' => 1))
            ->get()
            ->result_array();
    }
}
