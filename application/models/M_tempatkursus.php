<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tempatkursus extends MY_Model
{
    protected $table = 'tempatkursus';
    protected $schema = '';
    public $key = 'idtempatkursus';
    public $value = 'namakursus';

    function __construct()
    {
        parent::__construct();
    }

    public function getWithOwner()
    {
        $q = "SELECT * FROM pemilik p JOIN tempatkursus tk USING(idtempatkursus) JOIN jeniskursus jk ON tk.idjeniskursus=jk.idjeniskursus";
        return $this->db->query($q);
    }
}
