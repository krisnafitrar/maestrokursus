<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_user', 'users');
        $this->load->model('M_peserta', 'peserta');
    }

    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('home');
        }

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Login Page";
            $this->template->load('template_auth', 'auth/login', $data);
        } else {
            //validasinya success
            $this->login();
        }
    }


    private function login()
    {
        if ($this->session->userdata('username')) {
            redirect('user');
        }

        $username = $this->input->post('username');
        $password = $this->input->post('password');


        $user = $this->users->auth($username)->row_array();

        //jika usernya ada
        if ($user) {
            //jika usernya aktif
            if ($user['is_active'] == 1) {
                //cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'iduser' => $user['id'],
                        'username' => $user['username'],
                        'role_id' => $user['role_id'],
                        'email' => $user['email'],
                        'idpeserta' => $user['idpeserta'],
                        'idowner' => $user['idowner']
                    ];

                    $this->session->set_userdata($data);
                    if ($this->session->userdata['role_id'] < 3) {
                        redirect('home');
                    } else {
                        redirect('beranda');
                    }
                } else {
                    //password salah
                    setMessage('Password salah !', 'danger');
                    redirect('auth');
                }
            } else {
                //usernya tidak aktif
                setMessage('Akun anda telah di non-aktifkan', 'danger');
                redirect('auth');
            }
        } else {
            //usernya tidak ada
            setMessage('User tidak terdaftar', 'danger');
            redirect('auth');
        }
    }

    public function daftar()
    {
        $data['title'] = "Halaman Registrasi";
        $this->template->load('template_auth', 'auth/registration', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $notelp = $this->input->post('notelp');
            $password1 = $this->input->post('password1');
            $password2 = $this->input->post('password2');

            $a_user = [
                'name' => $nama,
                'username' => $username,
                'idpeserta' => $this->peserta->getAI(),
                'email' => $email,
                'image' => 'default.jpg',
                'password' => password_hash($password1, PASSWORD_DEFAULT),
                'role_id' => 3,
                'is_active' => 1,
                'date_created' => time()
            ];

            $a_peserta = [
                'namapeserta' => $nama,
                'email' => $email,
                'notelp' => $notelp,
            ];

            if ($password1 == $password2) {
                $this->peserta->beginTrans();
                $this->peserta->insert($a_peserta);
                $this->users->insert($a_user);
                $ok = $this->peserta->statusTrans();
                $this->peserta->commitTrans($ok);

                $ok && $ok ? setMessage('Yeay kamu berhasil mendaftar!', 'success') : setMessage('Gagal mendaftar!', 'danger');
            } else {
                setMessage('Password tidak cocok!', 'danger');
            }

            redirect('auth/daftar');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('idpeserta');
        $this->session->unset_userdata('idowner');

        setMessage('Berhasil logout!', 'success');
        redirect('beranda');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
