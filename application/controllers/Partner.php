<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Partner extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pemilik', 'pemilik');
        $this->load->model('M_tempatkursus', 'tempatkursus');
    }

    public function index()
    {
        $data['title'] = 'Daftar Partner';
        $data['nav'] = 'partner';
        $data['jeniskursus'] = $this->db->get('jeniskursus')->result_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/partner/index', $data);
        $this->load->view('client/inc/inc_footer', $data);

        if ($_POST || $_FILES) {
            $namapemilik = $this->input->post('namapemilik');
            $jk = $this->input->post('jk');
            $ttl = $this->input->post('ttl');
            $norek = $this->input->post('norek');
            $bank = $this->input->post('bank');
            $an_norek = $this->input->post('an_norek');
            $noidentitas = $this->input->post('noidentitas');
            $email = $this->input->post('email');
            $alamat = $this->input->post('alamat');
            $ktp = $_FILES['ktp']['name'];
            $namakursus = $this->input->post('namakursus');
            $idjenis = $this->input->post('idjenis');
            $noizin = $this->input->post('noizin');
            $emailcenter = $this->input->post('emailcenter');
            $callcenter = $this->input->post('callcenter');
            $namapemilik = $this->input->post('namapemilik');
            $kota = $this->input->post('kota');
            $provinsi = $this->input->post('provinsi');
            $alamatkursus = $this->input->post('alamatkursus');
            $fotokursus = $_FILES['fotokursus']['name'];

            if ($ktp) {
                $config['upload_path'] = './assets/img/datapartner/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('ktp')) {
                    $ktp = $this->upload->data('file_name');
                } else {
                    setMessage($this->upload->display_errors(), 'danger');
                }
            }

            if ($fotokursus) {
                $config['upload_path'] = './assets/img/datapartner/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('fotokursus')) {
                    $fotokursus = $this->upload->data('file_name');
                } else {
                    setMessage($this->upload->display_errors(), 'danger');
                }
            }


            $data_pemilik = [
                'noidentitas' => $noidentitas,
                'namapemilik' => $namapemilik,
                'jk' => $jk,
                'ttl' => $ttl,
                'email' => $email,
                'norekening' => $norek,
                'an_norek' => $an_norek,
                'bank' => $bank,
                'idtempatkursus' => $this->tempatkursus->getAI(),
                'alamat' => $alamat,
                'foto_ktp' => $ktp
            ];

            $data_kursus = [
                'namakursus' => $namakursus,
                'idjeniskursus' => $idjenis,
                'kota' => $kota,
                'provinsi' => $provinsi,
                'alamatkursus' => $alamatkursus,
                'callcenter' => $callcenter,
                'emailcenter' => $emailcenter,
                'noizin' => $noizin,
                'fotokursus' => $fotokursus,
                'tgldaftar' => time()
            ];

            $this->tempatkursus->beginTrans();
            $this->tempatkursus->insert($data_kursus);
            $this->pemilik->insert($data_pemilik);
            $status = $this->tempatkursus->statusTrans();
            $this->tempatkursus->commitTrans($status);

            $status && $status ? setMessage('Berhasil mengajukan pendaftaran partner, Tunggu balasan email dari kami dalam waktu 3 hari kerja!', 'success') : setMessage('Gagal mendaftar!', 'danger');
            redirect('partner');
        }
    }
}
