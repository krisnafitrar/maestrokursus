<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TempatKursus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_jeniskursus', 'jenis');

        //set default
        $this->title = 'Data Tempat Kursus';
        $this->menu = 'tempatkursus';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_jenis = $this->jenis->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namakursus', 'label' => 'Nama Lembaga'];
        $a_kolom[] = ['kolom' => 'noizin', 'label' => 'No Izin', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'idjeniskursus', 'label' => 'Jenis', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'kota', 'label' => 'Kota'];
        $a_kolom[] = ['kolom' => 'provinsi', 'label' => 'Provinsi'];
        $a_kolom[] = ['kolom' => 'fotokursus', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/datapartner/', 'file_type' => 'jpg|png|jpeg'];
        $a_kolom[] = ['kolom' => 'callcenter', 'label' => 'Call Center', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'emailcenter', 'label' => 'Email', 'type' => 'E'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
