<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Instrukturkursus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_paketkursus', 'paketkursus');
        $this->load->model('M_instruktur', 'instruktur');

        //set default
        $this->title = 'Manajemen Instruktur';
        $this->menu = 'instrukturkursus';
        $this->parent = 'manajemen';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        if (!empty($this->session->userdata('idowner'))) {
            $key = getCourse()->row_array()['idtempatkursus'];
            $this->cond = ['idtempatkursus' => $key];
            $a_tempat = $this->tempatkursus->getListCombo(['idtempatkursus' => $key]);
            $a_instruktur = $this->instruktur->getListCombo(['idtempatkursus' => $key]);
            $a_paket = $this->paketkursus->getListCombo(['idtempatkursus' => $key]);
        } else {
            $a_tempat = $this->tempatkursus->getListCombo();
            $a_instruktur = $this->instruktur->getListCombo();
            $a_paket = $this->paketkursus->getListCombo();
        }

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'idpaketkursus', 'label' => 'Paket Kursus', 'type' => 'S', 'option' => $a_paket];
        $a_kolom[] = ['kolom' => 'idinstruktur', 'label' => 'Instruktur', 'type' => 'S', 'option' => $a_instruktur];
        $a_kolom[] = ['kolom' => 'idtempatkursus', 'label' => 'Tempat Kursus', 'type' => 'S', 'option' => $a_tempat];


        $this->a_kolom = $a_kolom;
    }
}
