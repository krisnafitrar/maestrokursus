<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user', 'users');
        $this->load->model('M_peserta', 'peserta');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index()
    {
        $data['title'] = 'Profil';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'profil';
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/index', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function data()
    {
        $data['title'] = 'Data Peserta';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'data';
        $data['jeniskelamin'] = ['L' => 'Laki-Laki', 'P' => 'Perempuan'];
        $data['peserta'] = getPeserta()->row_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/datapeserta', $data);
        $this->load->view('client/inc/inc_footer', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $telepon = $this->input->post('telepon');
            $jk = $this->input->post('jk');
            $alamat = $this->input->post('alamat');
            $idpeserta = $this->input->post('idpeserta');

            $a_peserta = [
                'namapeserta' => $nama,
                'email' => $email,
                'notelp' => $telepon,
                'jeniskelamin' => $jk,
                'alamat' => $alamat
            ];
            $a_user = [
                'name' => $nama,
                'email' => $email
            ];

            $this->peserta->beginTrans();
            $this->peserta->update($a_peserta, $idpeserta);
            $this->users->update($a_user, $this->user['id']);
            $ok = $this->peserta->statusTrans();
            $this->peserta->commitTrans($ok);

            $ok && $ok ? setMessage('Berhasil mengubah data', 'success') : setMessage('Gagal mengubah data', 'danger');
            redirect('profil/data');
        }
    }

    public function password()
    {
        $data['title'] = 'Ganti Password';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'password';
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/password', $data);
        $this->load->view('client/inc/inc_footer', $data);

        if ($_POST) {
            $oldpass = $this->input->post('oldpassword');
            $newpass = $this->input->post('newpassword');
            $newpassconf = $this->input->post('newpasswordconf');

            if (password_verify($oldpass, $this->user['password'])) {
                if ($newpass == $newpassconf) {
                    if ($newpass != $oldpass) {
                        $update = $this->users->update(['password' => password_hash($newpass, PASSWORD_DEFAULT)], $this->user['id']);
                        $update ? setMessage('Berhasil merubah password', 'success') : setMessage('Gagal merubah password!', 'danger');
                    } else {
                        setMessage('Password tidak boleh sama dengan yang lama!', 'danger');
                    }
                } else {
                    setMessage('Password tidak cocok!', 'danger');
                }
            } else {
                setMessage('Password salah!', 'danger');
            }

            redirect('profil/password');
        }
    }
}
