<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');

        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengaturan', site_url('pengaturan'));

        $data['title'] = 'Pengaturan';
        $data['user'] = $this->user;

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'namakursus', 'label' => 'Nama Kursus'];
        $a_kolom[] = ['kolom' => 'callcenter', 'label' => 'Call Center'];
        $a_kolom[] = ['kolom' => 'emailcenter', 'label' => 'Email Center'];
        $a_kolom[] = ['kolom' => 'fotokursus', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/datapartner/'];

        $data['a_kolom'] = $a_kolom;
        $data['a_data'] = getCourse()->row_array();
        $this->template->load('template', 'pengaturan/index', $data);

        if ($_POST || $_FILES) {
            $key = $this->input->post('key');
            $col = $this->input->post('col');
            $data = $this->input->post('data');
            $file = $_FILES['file'];

            if ($file) {
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['upload_path'] = './assets/img/datapartner/';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    unlink(FCPATH . $config['upload_path'] . $data['a_data']['fotokursus']);
                    $file = $this->upload->data('file_name');
                } else {
                    setMessage($this->upload->display_errors(), 'danger');
                }
            }

            $a_kursus = [$col => empty($file) ? $data : $file];

            $ok = $this->tempatkursus->update($a_kursus, $key);
            $ok ? setMessage('Berhasil mengubah data', 'success') : setMessage('Gagal mengubah data!', 'danger');
            redirect('setting');
        }
    }
}
