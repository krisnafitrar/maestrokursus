<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!empty($_SESSION['user']))
            redirect('home');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function authLogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $this->load->model('M_users');
        $user = $this->M_users->getBy(array('username' => $username, 'password' => md5($password)))->row_array();
        if (!empty($user)) {
            $this->session->set_userdata(array('user' => $user));
            redirect('home');
        } else {
            $this->session->set_flashdata('error', 'Username atau password salah');
            redirect('site');
        }
    }
}
