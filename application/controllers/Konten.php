<?php

class Konten extends CI_Controller
{
    protected $user;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_role');
        $this->load->model('M_user', 'M_users');
        $this->load->model('M_menu');
        $this->load->model('M_access_menu');
        $this->user = $this->M_users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $data['title'] = 'Konten';
        $data['user'] = $this->user;
        $data['konten'] = $this->db->get('konten')->result_array();
        $this->template->load('template', 'konten/index', $data);

        if ($_POST) {
            $act = $this->input->post('act');
            $key = $this->input->post('key');
            $kode = $this->input->post('kodekonten');
            $judul = $this->input->post('judul');
            $dekripsi = $this->input->post('deskripsi');
            $url = $this->input->post('url');
            $gambar = $this->input->post('gambar');
            $infotambahan = $this->input->post('infotambahan');
            $is_show = 1;
            $idkonten = $this->input->post('idkonten');

            $data = [
                'idkonten' => $act == 'simpan' ? $idkonten : $key,
                'kode_konten' => $kode,
                'judul' => $judul,
                'deskripsi' => $dekripsi,
                'url' => $url,
                'gambar' => $gambar,
                'info_tambahan' => $infotambahan,
                'is_show' => $is_show
            ];

            switch ($act) {
                case 'simpan':
                    # code...
                    break;
                case 'edit':
                    $ok = $this->db->update('konten', $data, ['idkonten' => $key]);
                    $ok ? setMessage('Berhasil mengubah konten!', 'success') : setMessage('Gagal mengubah konten!', 'danger');
                    redirect('konten');
                    break;
            }
        }
    }

    public function getKonten($key)
    {
        echo json_encode($this->db->get_where('konten', ['idkonten' => $key])->row_array());
    }

    public function artikel()
    {
    }
}
