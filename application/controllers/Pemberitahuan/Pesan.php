<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pesan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Pesan Masuk';
        $this->menu = 'pesan';
        $this->parent = 'pemberitahuan';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'nama', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'email', 'label' => 'Email'];
        $a_kolom[] = ['kolom' => 'pesan', 'label' => 'Pesan', 'type' => 'A'];
        $a_kolom[] = ['kolom' => 'time', 'label' => 'Waktu'];

        $this->a_kolom = $a_kolom;
    }
}
