<?php

class Kontak extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_konten', 'konten');
        $this->load->model('M_pesan', 'pesan');
        $this->load->model('M_jeniskonten', 'jeniskonten');
    }

    public function index()
    {
        $data['title'] = 'Kontak kami';
        $data['nav'] = 'kontak';
        $data['users'] = $this->user;
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/contact/index', $data);
        $this->load->view('client/inc/inc_footer', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $pesan = $this->input->post('pesan');

            $a_data = [
                'nama' => $nama,
                'email' => $email,
                'pesan' => $pesan
            ];

            $ok = $this->pesan->insert($a_data);
            $ok ? setMessage('Berhasil mengirim pesan', 'success') : setMessage('Gagal mengirim pesan, coba lagi!', 'danger');
            redirect('kontak');
        }
    }
}
