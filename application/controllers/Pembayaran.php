<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pembayaran', 'pembayaran');
        $this->load->model('M_daftarpeserta', 'daftarpeserta');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pembayaran Peserta', site_url('pembayaran'));

        $data['title'] = 'Pembayaran Peserta';
        $data['menu_now'] = 'Pembayaran';
        $data['user'] = $this->user;

        if (!empty($this->session->userdata('idowner'))) {
            $key = getCourse()->row_array()['idtempatkursus'];
            $data['pembayaran'] = $this->pembayaran->getPembayaran($key)->result_array();
        } else {
            $data['pembayaran'] = $this->pembayaran->getPembayaran()->result_array();
        }
        $this->template->load('template', 'pembayaran/index', $data);
    }

    public function updateStatusPembayaran($status, $id, $key)
    {
        $this->pembayaran->beginTrans();
        $this->pembayaran->update(['status' => $status], $id);
        $this->daftarpeserta->update(['noregistrasi' => 'REG-' . time()], $key);
        $ok = $this->pembayaran->statusTrans();
        $this->pembayaran->commitTrans($ok);

        $ok && $ok ? setMessage('Berhasil mengubah status pembayaran', 'success') : setMessage('Gagal merubah status pembayaran', 'danger');
        redirect('pembayaran');
    }
}
