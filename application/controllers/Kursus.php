<?php

class Kursus extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('M_user', 'users');
        $this->load->model('M_paketkursus', 'paketkursus');
        $this->load->model('M_instrukturkursus', 'instrukturkursus');
        $this->load->model('M_komentar', 'komentar');
        $this->load->model('M_daftarpeserta', 'daftarpeserta');
        $this->load->model('M_pembayaran', 'pembayaran');
        $this->load->model('M_jeniskursus', 'jeniskursus');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index($id = null)
    {
        $data['title'] = 'Kursus';
        $data['nav'] = 'kursus';
        $data['active'] = 'all';
        $data['users'] = $this->user;
        $data['navbar'] = $this->jeniskursus->get()->result_array();
        $data['paketkursus'] = $this->paketkursus->getPaket()->result_array();
        if (!empty($id)) {
            $data['active'] = $id;
            $data['paketkursus'] = $this->paketkursus->getPaket($id, 'idjeniskursus')->result_array();
        }
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/kursus/index', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function detail($id)
    {
        $data['title'] = 'Kursus';
        $data['nav'] = 'kursus';
        $data['users'] = $this->user;
        $data['detail'] = $this->paketkursus->getPaket($id)->row_array();
        $data['instruktur'] = $this->instrukturkursus->getReference($id)->result_array();
        $data['komentar'] = $this->komentar->getKomentar($id)->result_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/kursus/detail', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function daftar()
    {
        $peserta = $this->input->post('peserta');
        $tempatkursus = $this->input->post('tempatkursus');
        $paketkursus = $this->input->post('paketkursus');
        $harga = $this->input->post('harga');
        $invoice = 'INV' . time();
        $expired = date('Y-m-d H:i:s', time() + (60 * 60 * intval(settingSIM()['expired_payment'])));

        $a_daftar = [
            'idtempatkursus' => $tempatkursus,
            'idpeserta' => $peserta,
            'idpaketkursus' => $paketkursus
        ];

        $a_pembayaran = [
            'invoice' => $invoice,
            'iddaftar' => $this->daftarpeserta->getAI(),
            'total' => $harga,
            'expired_at' => $expired,
            'status' => 0
        ];

        $check = $this->paketkursus->getBy(['idpaketkursus' => $paketkursus])->row_array();

        if ($check['kuota'] > 0) {
            $this->daftarpeserta->beginTrans();
            $this->daftarpeserta->insert($a_daftar);
            $this->pembayaran->insert($a_pembayaran);
            $this->paketkursus->update(['kuota' => $check['kuota'] - 1], $paketkursus);
            $ok = $this->daftarpeserta->statusTrans();
            $this->daftarpeserta->commitTrans($ok);
            if ($ok && $ok && $ok) {
                setMessage('Berhasil mendaftar kursus ' . $check['namapaketkursus'], 'success');
                redirect('kursus/pembayaran/' . $invoice);
            } else {
                setMessage('Gagal mendaftar kursus ' . $check['namapaketkursus'], 'danger');
                redirect('kursus/detail/' . $paketkursus);
            }
        } else {
            setMessage('Maaf kuota sudah penuh!', 'danger');
            redirect('kursus/detail/' . $paketkursus);
        }
    }

    public function pembayaran($inv)
    {
        $payment = $this->pembayaran->getBy(['invoice' => $inv]);

        if ($payment->num_rows() < 1) {
            redirect('kursus/mycourse');
        }

        $data['title'] = 'Pembayaran Pendaftaran';
        $data['nav'] = 'pembayaran';
        $data['users'] = $this->user;
        $data['detail'] = $payment->row_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/pembayaran/success', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function komentar()
    {
        $peserta = $this->input->post('peserta');
        $paketkursus = $this->input->post('paketkursus');
        $komentar = $this->input->post('komentar');

        if (!empty($peserta)) {
            $data = [
                'idpeserta' => $peserta,
                'idpaketkursus' => $paketkursus,
                'komentar' => $komentar
            ];
            $ok = $this->komentar->insert($data);
            $ok ? setMessage('Berhasil menulis komentar', 'success') : setMessage('Gagal menulis komentar', 'danger');
            redirect('kursus/detail/' . $paketkursus);
        } else {
            redirect('auth');
        }
    }

    public function hapuskomentar($key, $id)
    {
        $del = $this->komentar->delete($key);
        $del ? setMessage('Berhasil menghapus komentar', 'success') : setMessage('Gagal menghapus komentar!', 'danger');
        redirect('kursus/detail/' . $id);
    }

    public function myCourse()
    {
        $data['title'] = 'Kursus Saya';
        $data['nav'] = 'profil';
        $data['users'] = $this->user;
        $data['mycourse'] = $this->daftarpeserta->getKursus($this->session->userdata('idpeserta'))->result_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/kursus/mycourse', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function deletepeserta($id)
    {
        $this->pembayaran->beginTrans();
        $this->pembayaran->deleteBy(['iddaftar' => $id]);
        $this->daftarpeserta->delete($id);
        $ok = $this->pembayaran->statusTrans();
        $this->pembayaran->commitTrans($ok);

        $ok && $ok ? redirect('kursus/mycourse') : null;
    }

    public function detailmycourse($id = null)
    {
        $data['title'] = 'Detail kursus saya';
        $data['nav'] = 'account';
        $data['detail'] = $this->daftarpeserta->getReference($id)->row_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/kursus/detailmycourse', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function uploadBukti()
    {
        if ($_POST || $_FILES) {
            $idpembayaran = $this->input->post('idpembayaran');
            $iddaftar = $this->input->post('iddaftar');
            $oldbukti = $this->input->post('oldbukti');
            $foto = $_FILES['buktibayar'];
            $path = './assets/img/buktibayar';

            if ($foto) {
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('buktibayar')) {
                    unlink(FCPATH . $path . $oldbukti);
                    $foto = $this->upload->data('file_name');
                } else {
                    setMessage($this->upload->display_errors(), 'danger');
                }
            } else {
                setMessage('Anda belum upload bukti!', 'danger');
            }

            $record = ['buktibayar' => $foto];
            $ok = $this->pembayaran->update($record, $idpembayaran);
            $ok ? setMessage('Berhasil mengupload bukti, tunggu konfirmasi dari kami dalam beberapa jam kedepan', 'success') : setMessage('Gagal mengupload bukti!', 'danger');

            redirect('kursus/detailmycourse/' . $iddaftar);
        }
    }
}
