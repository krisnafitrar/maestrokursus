-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Nov 2019 pada 09.50
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simsekolah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `idguru` int(255) NOT NULL,
  `idmapel` int(11) NOT NULL,
  `namaguru` varchar(255) DEFAULT NULL,
  `jeniskelamin` varchar(10) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notlp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`idguru`, `idmapel`, `namaguru`, `jeniskelamin`, `nip`, `alamat`, `notlp`) VALUES
(2, 1, 'Krisna', 'L', '1011', 'JL.Diponegoro No.22', '08975424'),
(17, 0, 'Salma', 'P', '10012', 'Surabaya', '0823');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `idkelas` int(255) NOT NULL,
  `namakelas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`idkelas`, `namakelas`) VALUES
(1, 'X-Kimia Analys 1'),
(2, 'X-Kimia Analys 2'),
(3, 'X-Kimia Analys 3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelasajar`
--

CREATE TABLE `kelasajar` (
  `idkelasajar` int(11) NOT NULL,
  `idkelas` int(255) NOT NULL,
  `idtahun` varchar(5) NOT NULL,
  `walikelas` int(255) DEFAULT NULL,
  `namakelasajar` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelasajar`
--

INSERT INTO `kelasajar` (`idkelasajar`, `idkelas`, `idtahun`, `walikelas`, `namakelasajar`) VALUES
(1, 1, '20192', 2, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelassiswa`
--

CREATE TABLE `kelassiswa` (
  `idkelasajar` int(255) NOT NULL,
  `nis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelassiswa`
--

INSERT INTO `kelassiswa` (`idkelasajar`, `nis`) VALUES
(1, '10001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `idmapel` int(11) NOT NULL,
  `namamapel` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`idmapel`, `namamapel`) VALUES
(1, 'Agama Islam'),
(2, 'Fisika');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapelguru`
--

CREATE TABLE `mapelguru` (
  `nip` int(11) NOT NULL,
  `idmapel` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapelguru`
--

INSERT INTO `mapelguru` (`nip`, `idmapel`) VALUES
(1011, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `idjenispelanggaran` int(255) NOT NULL,
  `namapelanggaran` varchar(255) DEFAULT NULL,
  `idskor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `pelanggaran`
--

INSERT INTO `pelanggaran` (`idjenispelanggaran`, `namapelanggaran`, `idskor`) VALUES
(1, 'Merokok', 1),
(2, 'Mencuri', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggaransiswa`
--

CREATE TABLE `pelanggaransiswa` (
  `idpelanggaran` int(255) NOT NULL,
  `nis` varchar(255) DEFAULT NULL,
  `idtahun` varchar(5) DEFAULT NULL,
  `tgl_pelanggaran` date DEFAULT NULL,
  `idjenispelanggaran` int(255) DEFAULT NULL,
  `skor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `pelanggaransiswa`
--

INSERT INTO `pelanggaransiswa` (`idpelanggaran`, `nis`, `idtahun`, `tgl_pelanggaran`, `idjenispelanggaran`, `skor`) VALUES
(1, '10004', '20192', '2019-11-14', 1, 30),
(2, '10004', '20192', '2019-11-29', 1, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jeniskelamin` varchar(10) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notlp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `jeniskelamin`, `alamat`, `notlp`) VALUES
('10001', 'Ziya', 'P', 'Sini', '0897566'),
('10002', 'Krisna', 'L', 'JL.Kusnandar', '08975'),
('10003', 'Fitra', 'L', 'JL.Rajawali', '08986'),
('10004', 'Albu', 'L', 'JL.Kaliketek', '08898998'),
('10005', 'Salma', 'P', 'Surabaya', '089712');

-- --------------------------------------------------------

--
-- Struktur dari tabel `skor`
--

CREATE TABLE `skor` (
  `idskor` int(255) NOT NULL,
  `namaskor` varchar(255) DEFAULT NULL,
  `skormin` int(11) DEFAULT NULL,
  `skormax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `skor`
--

INSERT INTO `skor` (`idskor`, `namaskor`, `skormin`, `skormax`) VALUES
(1, 'Ringan', 5, 50),
(2, 'Sedang', 51, 100),
(4, 'Berat', 101, 200);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahunakademik`
--

CREATE TABLE `tahunakademik` (
  `idtahun` varchar(5) NOT NULL,
  `namatahun` varchar(255) DEFAULT NULL,
  `isaktif` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tahunakademik`
--

INSERT INTO `tahunakademik` (`idtahun`, `namatahun`, `isaktif`) VALUES
('20192', '2019/2020 Genap', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `idguru` varchar(128) DEFAULT NULL,
  `nis` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `idguru`, `nis`, `name`, `username`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(2, '2', '', 'Krisna Fitra', 'krisnafitra', 'krisnafitra@gmail.com', 'bm.jpg', '$2y$10$DIW6AyQvIhMx6QV9qdWP5OnASSWrmCqMkgaG0HsKGhp3e1Toj1gLS', 2, 1, 1574224996),
(9, '3', NULL, 'Salma', '10012', NULL, 'default.jpg', '$2y$10$q9rxtFe8ux6d7hVQYmhQp.2/GJ/2h4Vq/YMp7/aUCCyYQgrbx0Hr.', 3, 1, 1574930334);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 2, 1),
(3, 3, 2),
(4, 2, 3),
(7, 2, 2),
(9, 2, 4),
(11, 1, 2),
(12, 1, 3),
(13, 1, 5),
(14, 1, 9),
(15, 2, 5),
(17, 2, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `icon`) VALUES
(1, 'Home', 'fas fa-fw fa-home'),
(3, 'Pengaturan', 'fas fa-fw fa-cog'),
(5, 'Manajemen Sekolah', 'fas fa-fw fa-cogs'),
(9, 'Master', 'fas fa-fw fa-folder-open');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Headmaster'),
(2, 'Administrator'),
(3, 'Guru'),
(4, 'Siswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, '1', 'Dashboard', 'home', 'fas fa-fw fa-tachometer-alt', 1),
(2, '0', 'My Profile', 'user', 'fas fa-fw fa-user', 1),
(3, '3', 'Manajemen Menu', 'menu', 'fas fa-fw fa-folder', 1),
(4, '3', 'Manajemen Submenu', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(5, '9', 'Role', 'master/role', 'fas fa-fw fa-user-tie', 1),
(6, '0', 'Edit Profil', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(7, '9', 'Data Siswa', 'master', 'fas fa-fw fa-user-graduate', 1),
(8, '9', 'Data Guru', 'master/guru', 'fas fa-fw fa-chalkboard-teacher', 1),
(9, '9', 'Tahun Akademik', 'master/tahun', 'fas fa-fw fa-calendar-alt', 1),
(10, '9', 'Jenis Pelanggaran', 'master/pelanggaran', 'fas fa-fw fa-exclamation-circle', 1),
(11, '5', 'Pelanggaran Siswa', 'manajemen', 'fas fa-fw fa-user-ninja', 1),
(12, '9', 'Data Kelas', 'master/kelas', 'fas fa-fw fa-chalkboard', 1),
(14, '0', 'Ganti Password', 'user/changepassword', 'fas fa-fw fa-key', 1),
(15, '5', 'Kelas Ajar (Wali Kelas)', 'manajemen/kelasajar', 'fas fa-fw fa-chalkboard', 1),
(16, '9', 'Skor', 'master/skor', 'fas fa-fw fa-score', 1),
(17, '5', 'Kelas Siswa', 'manajemen/kelassiswa', 'fas fa-fw fa-chalkboard', 1),
(18, '5', 'Mata Pelajaran', 'manajemen/mapel', 'fas fa-fw fa-book', 1),
(20, '3', 'Users', 'menu/users', '', 1),
(21, '9', 'Data Mapel', 'master/mapel', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`idguru`) USING BTREE,
  ADD KEY `idmapel` (`idmapel`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`idkelas`) USING BTREE;

--
-- Indexes for table `kelasajar`
--
ALTER TABLE `kelasajar`
  ADD PRIMARY KEY (`idkelasajar`) USING BTREE,
  ADD KEY `fk_kelassiswa_tahun` (`idtahun`) USING BTREE,
  ADD KEY `fk_kelassiswa_guru` (`walikelas`) USING BTREE,
  ADD KEY `idkelas` (`idkelas`) USING BTREE;

--
-- Indexes for table `kelassiswa`
--
ALTER TABLE `kelassiswa`
  ADD KEY `fk_kelassiswa_ajar` (`idkelasajar`) USING BTREE,
  ADD KEY `fk_kelassiswa_siswa` (`nis`) USING BTREE;

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`idmapel`);

--
-- Indexes for table `mapelguru`
--
ALTER TABLE `mapelguru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`idjenispelanggaran`) USING BTREE,
  ADD KEY `fk_pelanggaran_skor` (`idskor`) USING BTREE;

--
-- Indexes for table `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  ADD PRIMARY KEY (`idpelanggaran`) USING BTREE,
  ADD KEY `fk_pelanggaransiswa_siswa` (`nis`) USING BTREE,
  ADD KEY `fk_pelanggaransiswa_tahun` (`idtahun`) USING BTREE,
  ADD KEY `fk_pelanggaransiswa_jenispelanggaran` (`idjenispelanggaran`) USING BTREE;

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`) USING BTREE;

--
-- Indexes for table `skor`
--
ALTER TABLE `skor`
  ADD PRIMARY KEY (`idskor`) USING BTREE;

--
-- Indexes for table `tahunakademik`
--
ALTER TABLE `tahunakademik`
  ADD PRIMARY KEY (`idtahun`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `nis` (`nis`),
  ADD KEY `idguru` (`idguru`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `idguru` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `idkelas` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kelasajar`
--
ALTER TABLE `kelasajar`
  MODIFY `idkelasajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `idmapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mapelguru`
--
ALTER TABLE `mapelguru`
  MODIFY `nip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1012;
--
-- AUTO_INCREMENT for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  MODIFY `idjenispelanggaran` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  MODIFY `idpelanggaran` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `skor`
--
ALTER TABLE `skor`
  MODIFY `idskor` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kelasajar`
--
ALTER TABLE `kelasajar`
  ADD CONSTRAINT `fk_kelassiswa_guru` FOREIGN KEY (`walikelas`) REFERENCES `guru` (`idguru`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelassiswa_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelassiswa`
--
ALTER TABLE `kelassiswa`
  ADD CONSTRAINT `fk_kelassiswa_ajar` FOREIGN KEY (`idkelasajar`) REFERENCES `kelasajar` (`idkelasajar`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelassiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD CONSTRAINT `fk_pelanggaran_skor` FOREIGN KEY (`idskor`) REFERENCES `skor` (`idskor`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  ADD CONSTRAINT `fk_pelanggaransiswa_jenispelanggaran` FOREIGN KEY (`idjenispelanggaran`) REFERENCES `pelanggaran` (`idjenispelanggaran`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pelanggaransiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pelanggaransiswa_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
