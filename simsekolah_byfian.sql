/*
 Navicat Premium Data Transfer

 Source Server         : Lokal MySQL
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : simsekolah

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 02/12/2019 11:46:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for guru
-- ----------------------------
DROP TABLE IF EXISTS `guru`;
CREATE TABLE `guru`  (
  `idguru` int(255) NOT NULL AUTO_INCREMENT,
  `namaguru` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `notlp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jeniskelamin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idguru`) USING BTREE,
  INDEX `idx_guru_nama`(`namaguru`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of guru
-- ----------------------------
INSERT INTO `guru` VALUES (2, 'Ahmad Dwi Alfian', '12121212', 'medokan', '081234567890', 'L');
INSERT INTO `guru` VALUES (4, 'Fian', '121212', 'Jalanin aja dulu', '081234567890', 'L');

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas`  (
  `idkelas` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namakelas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idkelas`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES ('1', 'Kelas 1');
INSERT INTO `kelas` VALUES ('2', 'Kelas 2');
INSERT INTO `kelas` VALUES ('3', 'Kelas 3');
INSERT INTO `kelas` VALUES ('4', 'Kelas 4');
INSERT INTO `kelas` VALUES ('5', 'Kelas 5');
INSERT INTO `kelas` VALUES ('6', 'Kelas 6');

-- ----------------------------
-- Table structure for kelasajar
-- ----------------------------
DROP TABLE IF EXISTS `kelasajar`;
CREATE TABLE `kelasajar`  (
  `idkelasajar` int(11) NOT NULL AUTO_INCREMENT,
  `idkelas` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idtahun` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namakelasajar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gurukelas` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idkelasajar`) USING BTREE,
  INDEX `idx_kelasajar_tahun`(`idtahun`) USING BTREE,
  INDEX `idx_kelasajar_guru`(`gurukelas`) USING BTREE,
  INDEX `iddx_kelasajar_idkelas`(`idkelas`) USING BTREE,
  CONSTRAINT `fk_kelasajar_guru` FOREIGN KEY (`gurukelas`) REFERENCES `guru` (`idguru`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_kelasajar_kelas` FOREIGN KEY (`idkelas`) REFERENCES `kelas` (`idkelas`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_kelasajar_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelasajar
-- ----------------------------
INSERT INTO `kelasajar` VALUES (1, '1', '20191', 'Kelas 1 20191', 4);

-- ----------------------------
-- Table structure for kelassiswa
-- ----------------------------
DROP TABLE IF EXISTS `kelassiswa`;
CREATE TABLE `kelassiswa`  (
  `idkelasajar` int(255) NOT NULL,
  `nis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  INDEX `fk_kelassiswa_ajar`(`idkelasajar`) USING BTREE,
  INDEX `fk_kelassiswa_siswa`(`nis`) USING BTREE,
  CONSTRAINT `fk_kelassiswa_ajar` FOREIGN KEY (`idkelasajar`) REFERENCES `kelasajar` (`idkelasajar`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_kelassiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelassiswa
-- ----------------------------
INSERT INTO `kelassiswa` VALUES (1, '10001');

-- ----------------------------
-- Table structure for mapel
-- ----------------------------
DROP TABLE IF EXISTS `mapel`;
CREATE TABLE `mapel`  (
  `idmapel` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namamapel` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idmapel`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mapel
-- ----------------------------
INSERT INTO `mapel` VALUES ('KIMIA001', 'KIMIA 1');

-- ----------------------------
-- Table structure for mapelguru
-- ----------------------------
DROP TABLE IF EXISTS `mapelguru`;
CREATE TABLE `mapelguru`  (
  `idmapel` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idguru` int(255) NOT NULL,
  PRIMARY KEY (`idmapel`, `idguru`) USING BTREE,
  INDEX `fk_mapelguru_guru`(`idguru`) USING BTREE,
  CONSTRAINT `fk_mapelguru_guru` FOREIGN KEY (`idguru`) REFERENCES `guru` (`idguru`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_mapelguru_mapel` FOREIGN KEY (`idmapel`) REFERENCES `mapel` (`idmapel`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mapelguru
-- ----------------------------
INSERT INTO `mapelguru` VALUES ('KIMIA001', 4);

-- ----------------------------
-- Table structure for pelanggaran
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaran`;
CREATE TABLE `pelanggaran`  (
  `idjenispelanggaran` int(255) NOT NULL AUTO_INCREMENT,
  `namapelanggaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idskor` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idjenispelanggaran`) USING BTREE,
  INDEX `fk_pelanggaran_skor`(`idskor`) USING BTREE,
  CONSTRAINT `fk_pelanggaran_skor` FOREIGN KEY (`idskor`) REFERENCES `skor` (`idskor`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pelanggaran
-- ----------------------------
INSERT INTO `pelanggaran` VALUES (2, 'Terlambat', 1);

-- ----------------------------
-- Table structure for pelanggaransiswa
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaransiswa`;
CREATE TABLE `pelanggaransiswa`  (
  `idpelanggaran` int(255) NOT NULL AUTO_INCREMENT,
  `nis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idtahun` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_pelanggaran` date NULL DEFAULT NULL,
  `idjenispelanggaran` int(255) NULL DEFAULT NULL,
  `skor` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idpelanggaran`) USING BTREE,
  INDEX `fk_pelanggaransiswa_siswa`(`nis`) USING BTREE,
  INDEX `fk_pelanggaransiswa_tahun`(`idtahun`) USING BTREE,
  INDEX `fk_pelanggaransiswa_jenispelanggaran`(`idjenispelanggaran`) USING BTREE,
  CONSTRAINT `fk_pelanggaransiswa_jenispelanggaran` FOREIGN KEY (`idjenispelanggaran`) REFERENCES `pelanggaran` (`idjenispelanggaran`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pelanggaransiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pelanggaransiswa_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pelanggaransiswa
-- ----------------------------
INSERT INTO `pelanggaransiswa` VALUES (1, '10001', '20191', '2019-11-26', 2, 3);
INSERT INTO `pelanggaransiswa` VALUES (2, '10001', '20191', '2019-11-27', 2, 3);

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa`  (
  `nis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `notlp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jeniskelamin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nis`) USING BTREE,
  INDEX `idx_siswa_nama`(`nama`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of siswa
-- ----------------------------
INSERT INTO `siswa` VALUES ('10001', 'Ziya', 'Sini', '0897566', 'P');
INSERT INTO `siswa` VALUES ('10002', 'Ziya', 'Sini', '0897566', 'P');
INSERT INTO `siswa` VALUES ('10003', 'Ziya', 'Sini', '0897566', 'P');
INSERT INTO `siswa` VALUES ('10004', 'Ziya', 'Sini', '0897566', 'P');
INSERT INTO `siswa` VALUES ('10005', 'Ziya', 'Sini', '0897566', 'P');
INSERT INTO `siswa` VALUES ('10006', 'Ziya', 'Sini', '0897566', 'P');

-- ----------------------------
-- Table structure for skor
-- ----------------------------
DROP TABLE IF EXISTS `skor`;
CREATE TABLE `skor`  (
  `idskor` int(255) NOT NULL AUTO_INCREMENT,
  `namaskor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `skormin` int(11) NULL DEFAULT NULL,
  `skormax` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idskor`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of skor
-- ----------------------------
INSERT INTO `skor` VALUES (1, 'Ringan', 1, 5);

-- ----------------------------
-- Table structure for tahunakademik
-- ----------------------------
DROP TABLE IF EXISTS `tahunakademik`;
CREATE TABLE `tahunakademik`  (
  `idtahun` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namatahun` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isaktif` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtahun`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tahunakademik
-- ----------------------------
INSERT INTO `tahunakademik` VALUES ('20182', '2018/2019 Genap', '1');
INSERT INTO `tahunakademik` VALUES ('20191', '2019/2020 Gasal', '1');
INSERT INTO `tahunakademik` VALUES ('20192', '2019/2020 Genap', '1');

-- ----------------------------
-- Table structure for user_access_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_access_menu`;
CREATE TABLE `user_access_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_uam_menu`(`menu_id`) USING BTREE,
  INDEX `fk_uam_role`(`role_id`) USING BTREE,
  CONSTRAINT `fk_uam_menu` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_uam_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_access_menu
-- ----------------------------
INSERT INTO `user_access_menu` VALUES (1, 2, 1);
INSERT INTO `user_access_menu` VALUES (4, 2, 3);
INSERT INTO `user_access_menu` VALUES (8, 2, 5);
INSERT INTO `user_access_menu` VALUES (11, 1, 5);

-- ----------------------------
-- Table structure for user_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_menu`;
CREATE TABLE `user_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `menu`(`menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_menu
-- ----------------------------
INSERT INTO `user_menu` VALUES (1, 'Master', 'fas fa-fw fa-database');
INSERT INTO `user_menu` VALUES (3, 'Pengaturan', 'fas fa-fw fa-tasks');
INSERT INTO `user_menu` VALUES (5, 'Manajemen Sekolah', 'fas fa-fw fa-cogs');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 'Kepala Sekolah');
INSERT INTO `user_role` VALUES (2, 'Administrator');
INSERT INTO `user_role` VALUES (3, 'Guru');
INSERT INTO `user_role` VALUES (4, 'Siswa');

-- ----------------------------
-- Table structure for user_sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_sub_menu`;
CREATE TABLE `user_sub_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_usm_menu`(`menu_id`) USING BTREE,
  CONSTRAINT `fk_usm_menu` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_sub_menu
-- ----------------------------
INSERT INTO `user_sub_menu` VALUES (3, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1);
INSERT INTO `user_sub_menu` VALUES (4, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1);
INSERT INTO `user_sub_menu` VALUES (5, 1, 'Role', 'master/role', 'fas fa-fw fa-user-tie', 1);
INSERT INTO `user_sub_menu` VALUES (7, 1, 'Data Siswa', 'master', 'fas fa-fw fa-user-graduate', 1);
INSERT INTO `user_sub_menu` VALUES (8, 1, 'Data Guru', 'master/guru', 'fas fa-fw fa-chalkboard-teacher', 1);
INSERT INTO `user_sub_menu` VALUES (9, 1, 'Tahun Akademik', 'master/tahun', 'fas fa-fw fa-calendar-alt', 1);
INSERT INTO `user_sub_menu` VALUES (11, 5, 'Pelanggaran Siswa', 'manajemen', 'fas fa-fw fa-user-ninja', 1);
INSERT INTO `user_sub_menu` VALUES (12, 1, 'Data Kelas', 'master/kelas', 'fas fa-fw fa-chalkboard', 1);
INSERT INTO `user_sub_menu` VALUES (14, 5, 'Kelas Ajar (Wali Kelas)', 'manajemen/kelasajar', 'fas fa-fw-files', 1);
INSERT INTO `user_sub_menu` VALUES (16, 1, 'Skor', 'master/skor', NULL, 1);
INSERT INTO `user_sub_menu` VALUES (17, 1, 'Jenis Pelanggaran', 'master/pelanggaran', NULL, 1);
INSERT INTO `user_sub_menu` VALUES (18, 1, 'Mata Pelajaran', 'master/mapel', NULL, 1);
INSERT INTO `user_sub_menu` VALUES (19, 3, 'User', 'menu/user', NULL, 1);
INSERT INTO `user_sub_menu` VALUES (20, 5, 'Mata Pelajaran Guru', 'manajemen/mapelguru', NULL, 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idguru` int(255) NULL DEFAULT NULL,
  `nis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_id` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_users_username`(`username`) USING BTREE,
  INDEX `idx_users_siswa`(`nis`) USING BTREE,
  INDEX `idx_users_guru`(`idguru`) USING BTREE,
  INDEX `idx_users_nama`(`name`) USING BTREE,
  INDEX `fk_users_role`(`role_id`) USING BTREE,
  CONSTRAINT `fk_users_guru` FOREIGN KEY (`idguru`) REFERENCES `guru` (`idguru`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_users_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_users_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'krisnafitrar', NULL, NULL, 'Krisna Fitra', 'krisnafitrar@gmail.com', 'bm1.jpg', '$2y$10$PVW4MxlCY30f.z9GubXQI.p4NYHntyDJ5oCg5bimkfiguGDUNp2cu', 2, 1, 1572863437);
INSERT INTO `users` VALUES (2, 'krisnafffr', NULL, NULL, 'Krisna', 'krisnafffr@gmail.com', 'IMG_20181110_063408_087.jpg', '$2y$10$DIW6AyQvIhMx6QV9qdWP5OnASSWrmCqMkgaG0HsKGhp3e1Toj1gLS', 2, 1, 1574224996);
INSERT INTO `users` VALUES (4, 'admin', 4, NULL, 'Administrator', NULL, 'default.jpg', '$2y$10$WeWNVXrFQBb6hRCZ4jNwc.2qGruKdP7TObYhzEKeJhTXpBWu7R4K.', 2, 1, 1575035808);

SET FOREIGN_KEY_CHECKS = 1;
